// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  api_backend: "http://localhost:8000/api/v1/chatbot/documents/",
  api_microsoft_graph: "https://graph.microsoft.com/v1.0/",
  redirect_url: "http://localhost:4200/",

  key_question: "ask-questions/",
  key_conversations: "conversations",

  token: "04ac74e162fd24b01aaf37971493282b52495904",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
