import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "", redirectTo: "home", pathMatch: "full",
  },
  {
    path: '',
    loadChildren: () => import('./modules/m-home-hm/m-home-hm.module').then(m => m.MHomeHmModule)
  },
  {
    path: '',
    loadChildren: () => import('./modules/m-support-st/m-support-st.module').then(m => m.MSupportStModule)
  },
  {
    path: "**", redirectTo: "no_found", pathMatch: "full",
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
