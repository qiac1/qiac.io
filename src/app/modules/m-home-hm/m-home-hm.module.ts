import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MHomeHmRoutingModule } from './m-home-hm-routing.module';
import { MHomeHmComponent } from './m-home-hm.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ElementsModule } from 'src/app/elements/elements.module';
import { HomeHmComponent } from './pages/home-hm/home-hm.component';
import { NgxTypewriterModule } from "ngx-typewriter";
import { ChatHmComponent } from './pages/chat-hm/chat-hm.component';


@NgModule({
  declarations: [
    MHomeHmComponent,
    HomeHmComponent,
    ChatHmComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TranslateModule,
    ElementsModule,
    NgxTypewriterModule,
    MHomeHmRoutingModule
  ]
})
export class MHomeHmModule { }
