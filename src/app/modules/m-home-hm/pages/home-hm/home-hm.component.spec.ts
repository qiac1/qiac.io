import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeHmComponent } from './home-hm.component';

describe('HomeHmComponent', () => {
  let component: HomeHmComponent;
  let fixture: ComponentFixture<HomeHmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeHmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeHmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
