import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { TPW, TPWInterface } from 'ngx-typewriter';
import { i_events_action, i_devices } from 'src/app/elements/interfaces/common';
import { CommonService } from 'src/app/elements/services/common/common.service';
import { EncryptStorage } from 'storage-encryption';

@Component({
  selector: 'app-home-hm',
  templateUrl: './home-hm.component.html',
  styleUrls: ['./home-hm.component.css']
})
export class HomeHmComponent implements OnInit {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  @Input() class!: string;
  @Input() id: string = "home_hm";
  @Input() location!: string;

  @Output() loaded: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();

  component!: any;
  html_elements: any = {};
  desktop_mode: boolean = false;

  show_loader: boolean = false;
  slogan_options: TPWInterface = {
    textList: [],
    speed: 80,
    loop: false,
    delay: 200,
  };

  constructor(
    private s_common: CommonService,
    private s_element_ref: ElementRef,
    private s_translate: TranslateService,
  ) {
    let tab_name: string = this.s_translate.instant("home.home.t0000") ?? "Home";
    this.s_common.change_name_tab(tab_name);
  }

  ngOnInit(): void {
    this.id = `c__${this.id}`;
    this.on_resize(null);

    let slogan = this.s_translate.instant("home.home.t0001") ?? "SQDM intelligent artificial chat.";
    this.slogan_options.textList = [slogan];
  }

  on_login(): void {
    this.show_loader = true;
    setTimeout(() => {
      let html: any = document.getElementsByTagName('html')[0] ?? null;
      html.style.backgroundImage = "";
      this.show_loader = false;
      this.s_common.go_to("chat");
    }, 400);
  };

  on_resize(event: any): void {
    let html: any = document.getElementsByTagName('html')[0] ?? null;
    html.style.backgroundImage = "url(./../../../../../../assets/images/home/00.jpg)";
    let device: i_devices = this.s_common.get_device();
    this.component = this.s_element_ref.nativeElement.children[0];
    this.component.name = this.id;
    let information: i_events_action = {
      dataset: {
        class: this.class,
        id: this.id,
        location: this.location,
      },
      element: this.component,
      event: "load",
      value: this.component,
    };
    this.desktop_mode = device.size.width >= 1152 ? true : false;
    this.loaded.emit(information);
    setTimeout(() => {
      let high_viewport: number = this.desktop_mode ? device.size.height : (device.size.height - 138);
      this.component.style.maxHeight = `${high_viewport}px`;
      this.component.children[0].style.height = this.desktop_mode ? `${(high_viewport - 64)}px` : null;
    }, 400);
  };
}
