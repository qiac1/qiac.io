import { Component, ElementRef, EventEmitter, Input, OnInit, Output, } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { TPWInterface } from "ngx-typewriter";
import { i_events_action, i_devices, i_file, } from "src/app/elements/interfaces/common";
import { i_conversation, i_request_ask_questions, i_answer, i_stages, } from "src/app/elements/interfaces/for_modules";
import { CommonService } from "src/app/elements/services/common/common.service";
import { ChatHmService } from "src/app/elements/services/for_modules/pages/chat-hm.service";
import { EncryptStorage } from "storage-encryption";

@Component({
  selector: "app-chat-hm",
  templateUrl: "./chat-hm.component.html",
  styleUrls: ["./chat-hm.component.css"],
})
export class ChatHmComponent implements OnInit {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  @Input() class!: string;
  @Input() id: string = "chat_hm";
  @Input() location!: string;

  @Output() loaded: EventEmitter<i_events_action> =
    new EventEmitter<i_events_action>();

  component!: any;
  html_elements: any = {};
  desktop_mode: boolean = false;

  conversation_list!: i_conversation[];
  current_conversation!: i_conversation;
  current_stage!: i_stages;
  extra_information_options: TPWInterface = {
    textList: [],
    speed: 32,
    loop: false,
    delay: 200,
  };
  hello_options: TPWInterface = {
    textList: [],
    speed: 80,
    loop: false,
    delay: 200,
  };
  information_options: TPWInterface = {
    textList: [],
    speed: 80,
    loop: false,
    delay: 200,
  };
  last_answer!: i_answer;
  last_upload_documento!: i_file;
  menu_mode: boolean = false;
  options_to_open: string = "";
  placeholder_input: string = "home.chat.t0013";
  question!: string;
  selected_document!: i_file;
  show_answers: boolean = false;
  show_loader: boolean = false;
  stage_list: i_stages[] = [];
  user_name: string = "common.t0023";
  window_open: boolean = false;
  writer_mode: boolean = false;
  number_of_windows_collapse: number = 0;

  constructor(
    private s_common: CommonService,
    private s_element_ref: ElementRef,
    private s_translate: TranslateService,
    private s_chat_hm: ChatHmService
  ) {
    let tab_name: string =
      this.s_translate.instant("home.chat.t0000") ?? "Home";
    this.s_common.change_name_tab(tab_name);
    this.stage_list = this.s_common.get_data_json("stages");
  }

  async get_conversation(): Promise<void> {
    let conversation_list: i_conversation[] =
      await this.s_chat_hm.get_conversations("");
    console.log(conversation_list);
    this.conversation_list = conversation_list;
  }

  ngOnInit(): void {
    this.id = `c__${this.id}`;
    this.on_resize(null);

    let user_name = this.s_translate.instant(this.user_name) ?? "Name here";
    let hello =
      this.s_translate.instant("home.chat.t0011") ??
      "Hola," + `<label>${user_name}</label>`;
    let information =
      this.s_translate.instant("home.chat.t0012") ?? "How can I help you?";
    let extra_information =
      this.s_translate.instant("home.chat.t0020") ??
      "You can ask me questions that do not require a context, you can also ask me questions based on my uploaded documents or you can upload a document and ask me questions about the uploaded document";
    this.hello_options.textList = [`${hello} <label>${user_name}</label>`];
    this.information_options.textList = [information];
    this.extra_information_options.textList = [extra_information];
    this.placeholder_input =
      this.s_translate.instant(this.placeholder_input) ??
      this.placeholder_input;
    this.current_stage = this.stage_list[0];
  }

  on_back(): void {
    let options_id = "";
    this.options_to_open = options_id;
    let tab_name: string =
      this.s_translate.instant("home.chat.t0000") ?? "Home";
    this.s_common.change_name_tab(tab_name);
  }

  on_close(): void {
    this.window_open = false;
    this.menu_mode = this.window_open;
    let options_id = "";
    this.options_to_open = options_id;
    let tab_name: string =
      this.s_translate.instant("home.chat.t0000") ?? "Home";
    this.s_common.change_name_tab(tab_name);
  }

  on_collapse(data: i_events_action): void {
    if (data.value) {
      this.number_of_windows_collapse = this.number_of_windows_collapse + 1;
    } else {
      this.number_of_windows_collapse = this.number_of_windows_collapse - 1;
    };
    if (this.number_of_windows_collapse > 3) {
      this.number_of_windows_collapse = 3;
    } if (this.number_of_windows_collapse < 0) {
      this.number_of_windows_collapse = 0;
    };
    data.value ? data.element.parentNode.classList.add("minimal") : data.element.parentNode.classList.remove("minimal");
  };

  on_off_menu(event: i_events_action): void {
    let tab_name: string = "";
    this.window_open = event.value ? true : false;
    this.menu_mode = this.window_open;
    this.options_to_open = !this.window_open ? "" : this.options_to_open;
    tab_name = this.window_open
      ? this.s_translate.instant("home.chat.t0004")
      : this.s_translate.instant("home.chat.t0000");
    this.s_common.change_name_tab(tab_name);
  }

  on_off_profile(): void {
    let tab_name: string = "";
    this.window_open = !this.window_open ? true : false;
    this.menu_mode = this.window_open;
    this.options_to_open = !this.window_open ? "" : this.options_to_open;
    tab_name = this.window_open
      ? this.s_translate.instant("home.chat.t0006")
      : this.s_translate.instant("home.chat.t0000");
    this.s_common.change_name_tab(tab_name);
    this.window_open ? this.on_open_menu_option("profile") : null;
  }

  async on_key_up(event: any): Promise<void> {
    this.show_loader = true;
    if (
      event.key === "Enter" ||
      event.code === "Enter" ||
      event.keyCode === 13
    ) {
      let answer_question: any = {};
      answer_question.question = event.target.value;
      let answer = <any>{};
      switch (this.current_stage.id) {
        case "1":
          answer_question.question = event.target.value;
          answer = await this.s_chat_hm.set_chat(answer_question);
          break;
        case "2":
          answer_question.document_id = "7";
          answer = await this.s_chat_hm.set_chat(answer_question);
          break;
        case "3":
          if (this.selected_document) {
            answer_question.document_id = this.selected_document.id;
            answer = await this.s_chat_hm.set_chat(answer_question);
          };
          break;
        default:
          break;
      }
      answer = this.current_stage.id == "1" ? answer[0] : this.current_stage.id == "2" ? answer[1] : answer[2];
      if (answer && answer.answer) {
        answer.question = answer.question ? answer.question : event.target.value;
        if (!this.current_conversation) {
          this.current_conversation = {
            actions: {
              read: true,
              update: true,
              delete: true,
            },
            creation_date: {
              placeholder: this.s_common.serialize_date(new Date(), "LLL"),
              value: new Date(),
            },
            id: this.s_common.guid_generator(),
            name: answer.question,
            answers: [],
          };
          this.current_conversation.answers.push(answer);
        } else {
          this.current_conversation.answers.push(answer);
        };
        this.last_answer = answer;
        this.show_loader = this.last_answer ? false : true;
        this.show_answers = true;
        this.question = "";
        event.target.value = "";
        setTimeout(() => {
          this.html_elements.requests =
            document.getElementById("answers") ?? null;
          this.html_elements.last_request =
            document.getElementById(`c__answer_${this.last_answer.id}`) ?? null;
          this.html_elements.requests.scrollTop = this.html_elements.requests
            ? this.html_elements.requests.scrollHeight +
            this.html_elements.last_request.clientHeight +
            10
            : null;
        }, 200);
      };
    };
  }

  async on_load_doc(data: any): Promise<void> {
    let file: any =
      data.target.files.length > 0 ? data.target.files[0] : null;
    if (file.type === "application/pdf") {
      console.log(file);
      /*this.selected_document = {
        blob: file,
        name: file.name,
      };*/
      if (this.selected_document) {
        if (this.selected_document.name == file.name) {
          this.selected_document = this.selected_document;
        } else {
          this.selected_document = {
            blob: file,
            name: file.name,
          };
        };
      } else {
        this.selected_document = {
          blob: file,
          name: file.name,
        };
      };
      if (this.selected_document) {
        let document: any = await this.s_chat_hm.set_document(this.selected_document);
        if (document) {
          this.selected_document = document;
          alert(this.s_translate.instant("for_modules.window_chat.t0013"));
        } else {
          alert(this.s_translate.instant("for_modules.window_chat.t0012"));
        };
      } else {
        alert(this.s_translate.instant("for_modules.window_chat.t0012"));
      };
    }
  }

  on_new_chat(): void {
    this.writer_mode = true;
  }

  on_open_menu_option(options_id: string): void {
    let tab_name: string = "";
    this.options_to_open = options_id;
    switch (options_id) {
      case "help_options":
        tab_name =
          this.s_translate.instant("for_modules.help.t0001") ??
          this.s_translate.instant("home.chat.t0000");
        this.s_common.change_name_tab(tab_name);
        break;
      case "profile":
        tab_name =
          this.s_translate.instant("home.chat.t0006") ??
          this.s_translate.instant("home.chat.t0000");
        this.s_common.change_name_tab(tab_name);
        break;
      case "settings_options":
        tab_name =
          this.s_translate.instant("for_modules.settings.t0001") ??
          this.s_translate.instant("home.chat.t0000");
        this.s_common.change_name_tab(tab_name);
        break;
      default:
        break;
    }
    this.session_data.encrypt("last_options_open", options_id);
  }

  on_resize(event: any): void {
    let html: any = document.getElementsByTagName("html")[0] ?? null;
    html.style.backgroundImage =
      "url(./../../../../../../assets/images/home/01.png)";
    let device: i_devices = this.s_common.get_device();
    this.component = this.s_element_ref.nativeElement.children[0];
    this.component.name = this.id;
    let information: i_events_action = {
      dataset: {
        class: this.class,
        id: this.id,
        location: this.location,
      },
      element: this.component,
      event: "load",
      value: this.component,
    };
    this.desktop_mode = device.size.width >= 1152 ? true : false;
    this.loaded.emit(information);
    setTimeout(() => {
      let high_viewport: number = this.desktop_mode
        ? device.size.height
        : device.size.height - 138;
      this.component.style.maxHeight = `${high_viewport}px`;
      //this.component.children[0].style.height = this.desktop_mode ? `${(high_viewport - 64)}px` : null;
    }, 400);
  }

  on_stage(stage: i_stages): void {
    for (let a = 0; a < this.stage_list.length; a++) {
      let stage: i_stages = this.stage_list[a];
      stage.selected = false;
    }
    stage.selected = true;
    this.current_stage = stage;
    this.current_conversation ? this.current_conversation.answers = [] : null;
  }

  async on_submit(): Promise<void> {
    if (this.question) {
      let answer_question: any = {};
      answer_question.question = this.question;
      let answer = <any>{};
      switch (this.current_stage.id) {
        case "1":
          answer_question.question = this.question;
          answer = await this.s_chat_hm.set_chat(answer_question);
          break;
        case "2":
          answer_question.document_id = "7";
          answer = await this.s_chat_hm.set_chat(answer_question);
          break;
        case "3":
          if (this.selected_document) {
            if (this.selected_document.blob) {
              if (!this.last_upload_documento) {
                this.last_upload_documento = await this.s_chat_hm.set_document(
                  this.selected_document
                );
              };
              answer_question.document_id = this.last_upload_documento.id ?? "8";
            } else {
              alert(this.s_translate.instant("control_exceptions.t0000"));
            }
          } else {
            alert(this.s_translate.instant("control_exceptions.t0000"));
          }
          break;
        default:
          break;
      }
      answer = this.current_stage.id == "1" ? answer[0] : this.current_stage.id == "2" ? answer[1] : answer[2];
      if (answer && answer.answer) {
        answer.question = answer.question ? answer.question : this.question;
        if (!this.current_conversation) {
          this.current_conversation = {
            actions: {
              read: true,
              update: true,
              delete: true,
            },
            creation_date: {
              placeholder: this.s_common.serialize_date(new Date(), "LLL"),
              value: new Date(),
            },
            id: this.s_common.guid_generator(),
            name: answer.question,
            answers: [],
          };
          this.current_conversation.answers.push(answer);
        } else {
          this.current_conversation.answers.push(answer);
        };
        this.last_answer = answer;
        this.show_loader = this.last_answer ? false : true;
        this.show_answers = true;
        this.question = "";
        setTimeout(() => {
          this.html_elements.requests =
            document.getElementById("answers") ?? null;
          this.html_elements.last_request =
            document.getElementById(`c__answer_${this.last_answer.id}`) ?? null;
          this.html_elements.requests.scrollTop = this.html_elements.requests
            ? this.html_elements.requests.scrollHeight +
            this.html_elements.last_request.clientHeight +
            10
            : null;
        }, 200);
      };
    };
  };
}
