import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatHmComponent } from './chat-hm.component';

describe('ChatHmComponent', () => {
  let component: ChatHmComponent;
  let fixture: ComponentFixture<ChatHmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChatHmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatHmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
