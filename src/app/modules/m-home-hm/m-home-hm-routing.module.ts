import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MHomeHmComponent } from './m-home-hm.component';
import { HomeHmComponent } from './pages/home-hm/home-hm.component';
import { ChatHmComponent } from './pages/chat-hm/chat-hm.component';

const routes: Routes = [{
  path: '',
  component: MHomeHmComponent,
  children: [
    { path: 'home', component: HomeHmComponent },
    { path: 'chat', component: ChatHmComponent },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MHomeHmRoutingModule { }
