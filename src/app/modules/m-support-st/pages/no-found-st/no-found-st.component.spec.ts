import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoFoundStComponent } from './no-found-st.component';

describe('NoFoundStComponent', () => {
  let component: NoFoundStComponent;
  let fixture: ComponentFixture<NoFoundStComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoFoundStComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoFoundStComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
