import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { i_events_action, i_devices } from 'src/app/elements/interfaces/common';
import { CommonService } from 'src/app/elements/services/common/common.service';
import { EncryptStorage } from 'storage-encryption';

@Component({
  selector: 'app-no-found-st',
  templateUrl: './no-found-st.component.html',
  styleUrls: ['./no-found-st.component.css']
})
export class NoFoundStComponent implements OnInit {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  @Input() class!: string;
  @Input() id: string = "no_found_st";
  @Input() location!: string;

  @Output() loaded: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();

  component!: any;
  html_elements: any = {};
  desktop_mode: boolean = false;

  constructor(
    private s_common: CommonService,
    private s_element_ref: ElementRef,
    private s_translate: TranslateService,
  ) {
    let tab_name: string = this.s_translate.instant("support.no_found.t0000") ?? "Not found";
    this.s_common.change_name_tab(tab_name);
  }

  ngOnInit(): void {
    this.id = `c__${this.id}`;
    this.on_resize(null);
  }

  on_resize(event: any): void {
    let device: i_devices = this.s_common.get_device();
    this.component = this.s_element_ref.nativeElement.children[0];
    this.component.name = this.id;
    let information: i_events_action = {
      dataset: {
        class: this.class,
        id: this.id,
        location: this.location,
      },
      element: this.component,
      event: "load",
      value: this.component,
    };
    this.desktop_mode = device.size.width >= 1152 ? true : false;
    this.loaded.emit(information);
    setTimeout(() => {
      let high_viewport: number = device.size.height;
      this.component.style.maxHeight = `${high_viewport}px`;
      this.component.children[0].style.height = this.desktop_mode ? `${(high_viewport - 64)}px` : "100%";
    }, 400);
  };
}
