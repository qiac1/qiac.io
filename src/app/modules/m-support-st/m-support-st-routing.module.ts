import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MSupportStComponent } from './m-support-st.component';
import { NoFoundStComponent } from './pages/no-found-st/no-found-st.component';

const routes: Routes = [{
  path: '',
  component: MSupportStComponent,
  children: [
    { path: 'no_found', component: NoFoundStComponent },],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MSupportStRoutingModule { }
