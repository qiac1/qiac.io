import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MSupportStRoutingModule } from './m-support-st-routing.module';
import { MSupportStComponent } from './m-support-st.component';
import { NoFoundStComponent } from './pages/no-found-st/no-found-st.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ElementsModule } from 'src/app/elements/elements.module';


@NgModule({
  declarations: [
    MSupportStComponent,
    NoFoundStComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TranslateModule,
    ElementsModule,
    MSupportStRoutingModule
  ]
})
export class MSupportStModule { }
