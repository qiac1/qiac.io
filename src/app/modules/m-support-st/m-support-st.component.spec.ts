import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MSupportStComponent } from './m-support-st.component';

describe('MSupportStComponent', () => {
  let component: MSupportStComponent;
  let fixture: ComponentFixture<MSupportStComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MSupportStComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MSupportStComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
