import { Component } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";
import { CommonService } from './elements/services/common/common.service';
import { EncryptStorage } from 'storage-encryption';
import { i_country, i_devices } from './elements/interfaces/common';
import { NgxDetectlocationModule } from "ngx-detectlocation";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  title = 'qaic';

  public language: any = ""; // Change to define any of the languages available to you.

  content!: any;
  show_alert: boolean = false;
  country_list: i_country[] = [];

  constructor(
    private s_translate: TranslateService,
    private s_common: CommonService,
    private s_http: HttpClient,
  ) {
    this.language = navigator.languages;
    this.language = this.language ? this.language.length > 0 ? this.language[0] : "" : "";
    this.language = this.language ? this.language.toLowerCase() : "";
    this.language = this.language ? this.language.split("-")[0] : "";
    this.s_translate.setDefaultLang('es');
    this.s_translate.use("es");
    let country_list: any = this.s_common.get_countries();
    this.country_list = country_list;
  }

  ngOnInit(): void {
    let devices: i_devices = this.s_common.get_device();
    document.body.classList.add(devices.device.os.toLowerCase());
    setTimeout(() => {
      this.content = document.getElementById("main_qaic") ?? null;
      this.content ? this.content.name = "main_qaic" : null;
    }, 400);

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.get_location(position.coords.latitude, position.coords.longitude).subscribe((response) => {
          for (let a = 0; a < this.country_list.length; a++) {
            const country: i_country = this.country_list[a];
            let language_in: string = country.code.toLowerCase();
            let language_out: string = response.sys.country.toLowerCase();
            if (language_in == language_out) {
              this.language = country.language.abbreviation.toLowerCase();
              break;
            };
          };
          this.session_data.encrypt("language", this.language);
        });
      });
    } else {
      this.language = navigator.languages;
      this.language = this.language ? this.language.length > 0 ? this.language[0] : "" : "";
      this.language = this.language ? this.language.toLowerCase() : "";
      this.language = this.language ? this.language.split("-")[0] : "";
    }
  };

  get_location(latitude: any, longitude: any): Observable<any> {
    return this.s_http.get<any>(
      "https://api.openweathermap.org/data/2.5/weather?lat=" + latitude + "&lon=" + longitude + "&appid=276bd156a13bc99e42de7688055a81f6&units=metric&lang=" + this.language
    );
  }
}
