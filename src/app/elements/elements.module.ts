import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import * as from_components from './components';
import { CommonDirective } from './directives/common.directive';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { NgxTypewriterModule } from 'ngx-typewriter';

@NgModule({
  declarations: [...from_components.components, CommonDirective,],
  imports: [
    CommonModule,
    HttpClientModule,
    TranslateModule,
    NgxTypewriterModule,
  ],
  exports: [
    CommonModule,
    HttpClientModule,
    TranslateModule,
    NgxTypewriterModule,
    ...from_components.components,
  ]
})
export class ElementsModule { }
