import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { TPWInterface } from 'ngx-typewriter';
import { i_events_action, i_devices } from 'src/app/elements/interfaces/common';
import { i_answer } from 'src/app/elements/interfaces/for_modules';
import { CommonService } from 'src/app/elements/services/common/common.service';
import { EncryptStorage } from 'storage-encryption';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  @Input() class!: string;
  @Input() id: string = "fm_request";
  @Input() location!: string;

  @Input() request: i_answer = {
    answer: "common.t0024",
    context: "common.t0026",
    id: this.s_common.guid_generator(),
    question: "common.t0025",
  };

  @Output() loaded: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();

  component!: any;
  html_elements: any = {};
  dataset: any = {};
  desktop_mode: boolean = false;

  context_mode: boolean = false;
  request_options: TPWInterface = {
    textList: [],
    speed: 16,
    loop: false,
    delay: 200,
  };

  constructor(
    private s_common: CommonService,
    private s_element_ref: ElementRef,
    private s_translate: TranslateService,
  ) { }

  ngOnInit(): void {
    this.id = `c__${this.id}`;
    this.on_resize(null);
    this.dataset = {
      class: this.class,
      id: this.id,
      location: this.location,
    };

    let request = this.s_translate.instant(this.request.answer) ?? this.request.answer;
    this.request.answer = request ?? "";
    this.request.answer = this.request.answer;
    this.request_options.textList = [request];
  }

  on_off_context(): void {
    setTimeout(() => {
      this.context_mode = !this.context_mode ? true : false;
      setTimeout(() => {
        this.html_elements.requests = document.getElementById("answers") ?? null;
        this.html_elements.requests.scrollTop = this.html_elements.requests ? this.html_elements.requests.scrollHeight + this.component.clientHeight + 10 : null;
      }, 200);
    }, 400);
  };

  on_resize(event: any): void {
    let device: i_devices = this.s_common.get_device();
    this.component = this.s_element_ref.nativeElement.children[0];
    this.component.name = this.id;
    let information: i_events_action = {
      dataset: this.dataset,
      element: this.component,
      event: "load",
      value: this.component,
    };
    this.desktop_mode = device.size.width >= 1152 ? true : false;
    this.loaded.emit(information);
    /*setTimeout(() => {
      let high_viewport: number = device.size.height;
      this.component.style.maxHeight = `${high_viewport}px`;
      this.component.children[0].style.height = this.desktop_mode ? `${(high_viewport - 64)}px` : null;
    }, 400);*/
  };
}
