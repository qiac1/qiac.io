import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { TPWInterface } from 'ngx-typewriter';
import { i_events_action, i_devices, i_file } from 'src/app/elements/interfaces/common';
import { i_answer, i_conversation, i_document } from 'src/app/elements/interfaces/for_modules';
import { CommonService } from 'src/app/elements/services/common/common.service';
import { ChatHmService } from 'src/app/elements/services/for_modules/pages/chat-hm.service';
import { EncryptStorage } from 'storage-encryption';

@Component({
  selector: 'app-window-chat',
  templateUrl: './window-chat.component.html',
  styleUrls: ['./window-chat.component.css']
})
export class WindowChatComponent implements OnInit {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  @Input() class!: string;
  @Input() id: string = "fm_window_chat";
  @Input() location!: string;
  @Input() stage!: string;

  @Output() loaded: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();
  @Output() collapse: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();

  component!: any;
  html_elements: any = {};
  dataset: any = {};
  desktop_mode: boolean = false;

  collapse_bookstore: boolean = false;
  collapse_window: boolean = false;
  current_conversation!: i_conversation;
  description_chat: TPWInterface = {
    textList: [],
    speed: 80,
    loop: false,
    delay: 200,
  };
  documents_list!: i_document[];
  uploaded_document!: i_file;
  uploaded_file!: i_file;
  last_answer!: i_answer;
  question!: any;
  selected_library_document!: i_document;
  show_answers: boolean = false;
  show_loader: boolean = false;

  constructor(
    private s_common: CommonService,
    private s_element_ref: ElementRef,
    public s_translate: TranslateService,
    private s_chat_hm: ChatHmService
  ) { }

  async get_answer(question: string): Promise<void> {
    let answer_question: any = {};
    answer_question.question = question;
    let answer = <any>{};
    switch (this.stage) {
      case "1":
        answer_question.question = question;
        answer = await this.s_chat_hm.set_chat(answer_question);
        break;
      case "2":
        answer_question.document_id = this.selected_library_document.id;
        answer = await this.s_chat_hm.set_chat(answer_question);
        break;
      case "3":
        if (this.uploaded_file) {
          answer_question.document_id = this.uploaded_file.id;
          answer = await this.s_chat_hm.set_chat(answer_question);
        };
        break;
      default:
        break;
    };
    //answer = this.stage == "1" ? answer[0] : this.stage == "2" ? answer[1] : answer[2];
    if (answer && answer.answer) {
      answer.question = answer.question ? answer.question : question;
      if (!this.current_conversation) {
        this.current_conversation = {
          actions: {
            read: true,
            update: true,
            delete: true,
          },
          creation_date: {
            placeholder: this.s_common.serialize_date(new Date(), "LLL"),
            value: new Date(),
          },
          id: this.s_common.guid_generator(),
          name: answer.question,
          answers: [],
        };
        this.current_conversation.answers.push(answer);
      } else {
        this.current_conversation.answers.push(answer);
      };
      this.last_answer = answer;
      this.show_loader = this.last_answer ? false : true;
      this.show_answers = true;
      this.question = "";
      setTimeout(() => {
        this.html_elements.answers = document.getElementById(`answers_${this.id}`) ?? null;
        this.html_elements.last_answer = document.getElementById(`c__answer_${this.last_answer.id}`) ?? null;
        this.html_elements.answers.scrollTop = this.html_elements.answers ? this.html_elements.answers.scrollHeight + this.html_elements.last_answer.clientHeight + 10 : null;
      }, 200);
    } else {
      switch (this.stage) {
        case "1":
          alert(this.s_translate.instant("for_modules.window_chat.t0014"));
          break;
        case "2":
          alert(this.s_translate.instant("for_modules.window_chat.t0015"));
          break;
        case "3":
          alert(this.s_translate.instant("for_modules.window_chat.t0016"));
          break;
        default:
          break;
      }
    };
  };

  async get_documents_list(): Promise<void> {
    let documents_list: any = await this.s_chat_hm.get_documents();
    documents_list = this.s_common.sort_ascent(documents_list, "name");
    this.documents_list = documents_list;
    this.documents_list[0].selected = true;
    this.selected_library_document = this.documents_list[0];
    console.log(this.documents_list);
  };

  ngOnInit(): void {
    this.id = `c__${this.id}`;
    this.on_resize(null);
    this.dataset = {
      class: this.class,
      id: this.id,
      location: this.location,
    };

    if (this.stage == "2") {
      this.get_documents_list();
    };
  }

  on_key_up(event: any): void {
    if (event.key === "Enter" || event.code === "Enter" || event.keyCode === 13) {
      this.question = event.target.value ?? "";
      this.show_loader = true;
      this.get_answer(this.question);
      event.target.value = "";
    };
  };

  async on_load_document(data: any): Promise<void> {
    let file: any = data.target.files.length > 0 ? data.target.files[0] : null;
    if (file.type === "application/pdf") {
      if (this.uploaded_document) {
        if (this.uploaded_document.name == file.name) {
          this.uploaded_document = this.uploaded_document;
        } else {
          this.uploaded_document = {
            blob: file,
            name: file.name,
          };
        };
      } else {
        this.uploaded_document = {
          blob: file,
          name: file.name,
        };
      };
      if (this.uploaded_document) {
        let document: any = await this.s_chat_hm.set_document(this.uploaded_document);
        if (document) {
          this.uploaded_file = document;
          alert(this.s_translate.instant("for_modules.window_chat.t0013"));
        } else {
          alert(this.s_translate.instant("for_modules.window_chat.t0012"));
        };
      } else {
        alert(this.s_translate.instant("for_modules.window_chat.t0012"));
      };
    };
  }

  on_collapse_bookstore(): void {
    this.collapse_bookstore = !this.collapse_bookstore ? true : false;
  };

  on_collapse_window(data: i_events_action): void {
    this.collapse_window = data.value;
    let information: i_events_action = {
      dataset: this.dataset,
      element: this.component,
      event: "load",
      value: this.collapse_window,
    };
    this.collapse.emit(information);
  };

  on_resize(event: any): void {
    let device: i_devices = this.s_common.get_device();
    this.component = this.s_element_ref.nativeElement.children[0];
    this.component.name = this.id;
    let information: i_events_action = {
      dataset: this.dataset,
      element: this.component,
      event: "load",
      value: this.component,
    };
    this.desktop_mode = device.size.width >= 1152 ? true : false;
    this.loaded.emit(information);
    /*setTimeout(() => {
      let high_viewport: number = device.size.height;
      this.component.style.maxHeight = `${high_viewport}px`;
      this.component.children[0].style.height = this.desktop_mode ? `${(high_viewport - 64)}px` : null;
    }, 400);*/
  };

  on_select_document_bookstore(document: i_document): void {
    setTimeout(() => {
      for (let a = 0; a < this.documents_list.length; a++) {
        let document: i_document = this.documents_list[a];
        document.selected = false;
      };
      document.selected = true;
      this.selected_library_document = document;
    }, 400);
  };

  on_submit(): void {
    let input: any = document.getElementById(`inp_${this.stage}`) ?? null;
    if (input) {
      this.question = input.value ?? "";
      this.show_loader = true;
      this.get_answer(this.question);
      input.value = "";
    };
  };
}
