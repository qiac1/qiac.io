import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { i_events_action, i_devices } from 'src/app/elements/interfaces/common';
import { CommonService } from 'src/app/elements/services/common/common.service';
import { EncryptStorage } from 'storage-encryption';

@Component({
  selector: 'app-help-menu',
  templateUrl: './help-menu.component.html',
  styleUrls: ['./help-menu.component.css']
})
export class HelpMenuComponent implements OnInit {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  @Input() class!: string;
  @Input() id: string = "fm_help_menu";
  @Input() location!: string;

  @Output() back: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();
  @Output() close: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();
  @Output() open: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();
  @Output() loaded: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();

  component!: any;
  html_elements: any = {};
  dataset: any = {};
  desktop_mode: boolean = false;

  constructor(
    private s_common: CommonService,
    private s_element_ref: ElementRef,
    private s_translate: TranslateService,
  ) { }

  ngOnInit(): void {
    this.id = `c__${this.id}`;
    this.on_resize(null);
    this.dataset = {
      class: this.class,
      id: this.id,
      location: this.location,
    };
  }

  on_back(): void {
    let information: i_events_action = {
      dataset: this.dataset,
      element: this.component,
      event: "back",
      value: this.component,
    };
    this.back.emit(information);
  };

  on_close(): void {
    let information: i_events_action = {
      dataset: this.dataset,
      element: this.component,
      event: "close",
      value: this.component,
    };
    this.close.emit(information);
  };

  on_open(path: string): void {
    let information: i_events_action = {
      dataset: this.dataset,
      element: this.component,
      event: "open",
      value: path,
    };
    this.open.emit(information);
  };

  on_resize(event: any): void {
    let device: i_devices = this.s_common.get_device();
    this.component = this.s_element_ref.nativeElement.children[0];
    this.component.name = this.id;
    let information: i_events_action = {
      dataset: this.dataset,
      element: this.component,
      event: "load",
      value: this.component,
    };
    this.desktop_mode = device.size.width >= 1152 ? true : false;
    this.loaded.emit(information);
    /*setTimeout(() => {
      let high_viewport: number = device.size.height;
      this.component.style.maxHeight = `${high_viewport}px`;
      this.component.children[0].style.height = this.desktop_mode ? `${(high_viewport - 64)}px` : null;
    }, 400);*/
  };
}


