import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { i_events_action, i_devices } from 'src/app/elements/interfaces/common';
import { i_conversation } from 'src/app/elements/interfaces/for_modules';
import { CommonService } from 'src/app/elements/services/common/common.service';
import { EncryptStorage } from 'storage-encryption';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.css']
})
export class ConversationComponent implements OnInit {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  @Input() class!: string;
  @Input() id: string = "fm_conversation";
  @Input() location!: string;

  @Input() conversation: i_conversation = {
    actions: {
      delete: true,
      read: true,
      update: true,
    },
    creation_date: {
      placeholder: this.s_common.serialize_date(new Date(), ""),
      value: new Date(),
    },
    id: this.s_common.guid_generator(),
    name: this.s_common.generate_name_sqdm("GOP", "Component", "conversation_"),
    answers: [],
    show_hide: true,
  };

  @Output() delete: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();
  @Output() loaded: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();
  @Output() read: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();
  @Output() update: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();

  component!: any;
  html_elements: any = {};
  dataset: any = {};
  desktop_mode: boolean = false;

  constructor(
    private s_common: CommonService,
    private s_element_ref: ElementRef,
    private s_translate: TranslateService,
  ) { }

  ngOnInit(): void {
    this.id = `c__${this.id}`;
    this.on_resize(null);
    this.dataset = {
      class: this.class,
      conversation: this.conversation,
      id: this.id,
      location: this.location,
    };
  }

  on_delete(): void {
    let to_delete = confirm(this.s_translate.instant("for_modules.conversations.t0001"));
    let information: i_events_action = {
      dataset: this.dataset,
      element: this.component,
      event: "delete",
      value: to_delete,
    };
    this.delete.emit(information);
  };

  on_read(): void {
    let information: i_events_action = {
      dataset: this.dataset,
      element: this.component,
      event: "read",
      value: this.component,
    };
    this.read.emit(information);
  };

  on_resize(event: any): void {
    let device: i_devices = this.s_common.get_device();
    this.component = this.s_element_ref.nativeElement.children[0];
    this.component.name = this.id;
    let information: i_events_action = {
      dataset: this.dataset,
      element: this.component,
      event: "load",
      value: this.component,
    };
    this.desktop_mode = device.size.width >= 1152 ? true : false;
    this.loaded.emit(information);
    /*setTimeout(() => {
      let high_viewport: number = device.size.height;
      this.component.style.maxHeight = `${high_viewport}px`;
      this.component.children[0].style.height = this.desktop_mode ? `${(high_viewport - 64)}px` : null;
    }, 400);*/
  };

  on_update(): void {
    let to_update = prompt(this.s_translate.instant("for_modules.conversations.t0000", this.conversation.name));
    let information: i_events_action = {
      dataset: this.dataset,
      element: this.component,
      event: "update",
      value: to_update,
    };
    this.update.emit(information);
  };
}
