import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { i_events_action, i_devices } from 'src/app/elements/interfaces/common';
import { CommonService } from 'src/app/elements/services/common/common.service';
import { EncryptStorage } from 'storage-encryption';

@Component({
  selector: 'app-loader-conversation',
  templateUrl: './loader-conversation.component.html',
  styleUrls: ['./loader-conversation.component.css']
})
export class LoaderConversationComponent implements OnInit {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  @Input() class!: string;
  @Input() id: string = "loader_conversation";

  @Output() loaded: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();

  component!: any;
  html_elements: any = {};
  dataset: any = {};
  desktop_mode: boolean = false;
  number_texts: {
    width: string;
    margin: string;
  }[] = [];
  number_titles: {
    width: string;
    margin: string;
  }[] = [];
  text_line_number: number = 1;
  title_line_number: number = 1;
  width_random: any;

  constructor(
    private s_common: CommonService,
    private s_element_ref: ElementRef,
    private s_translate: TranslateService,
  ) {
    const width_random = Math.floor(Math.random() * (100 - 60)) + 60;
    this.width_random = {
      "width": `${width_random}%`,
    };
    this.text_line_number = Math.floor(Math.random() * 2);
    this.title_line_number = Math.floor(Math.random() * 3);
  }

  ngOnInit(): void {
    this.id = `uiux__${this.id}`;
    this.dataset = {
      class: this.class,
      id: this.id,
    };
    this.on_resize(null);
    this.text_line_number = this.text_line_number == 0 ? 1 : this.text_line_number;
    this.title_line_number = this.title_line_number == 0 ? 1 : this.title_line_number;
    for (let a = 0; a < this.text_line_number; a++) {
      const width = Math.floor(Math.random() * (100 - 60)) + 60;
      this.number_texts.push({
        width: `${width}%`,
        margin: !this.desktop_mode ? `0px 0px 0px calc(100% - ${width}%)` : `0px calc(100% - ${width}%) 0px 0px`,
      });
    };
    for (let a = 0; a < this.title_line_number; a++) {
      const width = Math.floor(Math.random() * (100 - 60)) + 60;
      this.number_titles.push({
        width: `${width}%`,
        margin: !this.desktop_mode ? `0px 0px 4px calc(100% - ${width}%)` : `0px calc(100% - ${width}%) 4px 0px`,
      });
    };
  }

  on_resize(event: any): void {
    let device: i_devices = this.s_common.get_device();
    this.component = this.s_element_ref.nativeElement.children[0];
    this.component.name = this.id;
    let information: i_events_action = {
      dataset: this.dataset,
      element: this.component,
      event: "load",
      value: this.component,
    };
    this.desktop_mode = device.size.width >= 1152 ? true : false;
    this.loaded.emit(information);
    /*setTimeout(() => {
      let high_viewport: number = device.size.height;
      this.component.style.maxHeight = `${high_viewport}px`;
    }, 400);*/
  };
}
