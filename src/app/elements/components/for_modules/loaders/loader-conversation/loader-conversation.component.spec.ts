import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoaderConversationComponent } from './loader-conversation.component';

describe('LoaderConversationComponent', () => {
  let component: LoaderConversationComponent;
  let fixture: ComponentFixture<LoaderConversationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoaderConversationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoaderConversationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
