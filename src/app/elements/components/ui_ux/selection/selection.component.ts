import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { i_events_action, i_devices, i_options } from 'src/app/elements/interfaces/common';
import { CommonService } from 'src/app/elements/services/common/common.service';
import { EncryptStorage } from 'storage-encryption';

@Component({
  selector: 'app-selection',
  templateUrl: './selection.component.html',
  styleUrls: ['./selection.component.css']
})
export class SelectionComponent implements OnInit {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  @Input() class!: string;
  @Input() id: string = "selection";

  @Input() deactivated: boolean = false;
  @Input() name!: string;
  @Input() placeholder: string = "common.t0013";
  @Input() options: i_options[] = [];
  @Input() radio_border: string = "var(--spacing_xs)";
  @Input() required: boolean = false;
  @Input() style: string = "white";
  @Input() tooltip!: string;

  @Output() loaded: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();
  @Output() selected: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();

  component!: any;
  html_elements: any = {};
  dataset: any = {};
  desktop_mode: boolean = false;

  css_styles = {
    "border-radius": "var(--spacing_xs)",
  };
  selected_value: string = "";
  selection_type: string = "only_text";
  default_option: i_options = {
    placeholder: "common.t0013",
    value: "",
  }

  constructor(
    private s_common: CommonService,
    private s_element_ref: ElementRef,
    private s_translate: TranslateService,
  ) { }

  ngOnInit(): void {
    this.id = `uiux__${this.id}`;
    this.dataset = {
      class: this.class,
      deactivated: this.deactivated,
      id: this.id,
      name: this.name,
      placeholder: this.placeholder,
      options: this.options,
      radio_border: this.radio_border,
      required: this.required,
      style: this.style,
      tooltip: this.tooltip,
    };
    this.on_resize(null);

    this.css_styles['border-radius'] = this.radio_border;
    this.default_option.placeholder = this.placeholder ?? this.default_option.placeholder;
    this.tooltip = this.tooltip ?? this.placeholder;
    this.options = this.options.length > 0 ? this.options : [];
    this.options.unshift(this.default_option);
    setTimeout(() => {
      this.html_elements.selection = document.getElementById(`select_${this.id}`) ?? null;
      for (let a = 0; a < this.options.length; a++) {
        const option: i_options = this.options[a];
        if (option.selected) {
          this.html_elements.selection.value = option.value;
          this.html_elements.content_selection = document.getElementById(`content_selection_${this.id}`) ?? null;
          this.html_elements.content_selection.style.borderColor = "var(--black)";
          break;
        } else {
          this.html_elements.selection.value = "";
        }
      }
    }, 400);
  }

  on_select(data: any): void {
    let option: i_options = this.s_common.search_exactly_in_array(this.options, "value", data.currentTarget.value);
    for (let a = 0; a < this.options.length; a++) {
      let option: i_options = this.options[a];
      option.selected = false;
      this.html_elements.content_selection = document.getElementById(`content_selection_${this.id}`) ?? null;
      this.html_elements.content_selection.style.borderColor = "var(--borders)";
    };
    option.selected = true;
    this.html_elements.content_selection = document.getElementById(`content_selection_${this.id}`) ?? null;
    this.html_elements.content_selection.style.borderColor = "var(--black)";
    if (!this.deactivated) {
      setTimeout(() => {
        let information: i_events_action = {
          dataset: this.dataset,
          element: this.component,
          event: "selected",
          value: option,
        };
        this.selected.emit(information);
      }, 400);
    }
  };

  on_resize(event: any): void {
    let device: i_devices = this.s_common.get_device();
    this.component = this.s_element_ref.nativeElement.children[0];
    this.component.name = this.id;
    let information: i_events_action = {
      dataset: this.dataset,
      element: this.component,
      event: "load",
      value: this.component,
    };
    this.desktop_mode = device.size.width >= 720 ? true : false;
    this.loaded.emit(information);
    /*setTimeout(() => {
      let high_viewport: number = device.size.height;
      this.component.style.maxHeight = `${high_viewport}px`;
    }, 400);*/
  };
}
