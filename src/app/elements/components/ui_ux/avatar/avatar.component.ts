import { Component, ElementRef, EventEmitter, Input, OnInit, Output, } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { i_events_action, i_devices } from 'src/app/elements/interfaces/common';
import { CommonService } from 'src/app/elements/services/common/common.service';
import { EncryptStorage } from 'storage-encryption';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.css']
})
export class AvatarComponent implements OnInit {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  @Input() class!: string;
  @Input() id: string = "avatar";

  @Input() deactivated: boolean = false;
  @Input() photo: string = "./../../../../../assets/images/home/02.png";
  @Input() radio_border: string = "var(--spacing_xs)";
  @Input() tooltip!: string;

  @Output() loaded: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();
  @Output() clicked: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();

  component!: any;
  html_elements: any = {};
  dataset: any = {};
  desktop_mode: boolean = false;

  css_styles = {
    "border-radius": "var(--spacing_xs)",
    "background-image": "url(./../../../../../assets/images/home/02.png)",
  };

  constructor(
    private s_common: CommonService,
    private s_element_ref: ElementRef,
    private s_translate: TranslateService,
  ) { }

  ngOnInit(): void {
    this.id = `uiux__${this.id}`;
    this.dataset = {
      class: this.class,
      deactivated: this.deactivated,
      id: this.id,
      radio_border: this.radio_border,
      tooltip: this.tooltip,
    };
    this.on_resize(null);

    this.css_styles['border-radius'] = this.radio_border;
    this.css_styles['background-image'] = `url(${this.photo})`;
  }

  on_click(): void {
    if (!this.deactivated) {
      setTimeout(() => {
        let information: i_events_action = {
          dataset: this.dataset,
          element: this.component,
          event: "click",
          value: true,
        };
        this.clicked.emit(information);
      }, 400);
    }
  };

  on_resize(event: any): void {
    let device: i_devices = this.s_common.get_device();
    this.component = this.s_element_ref.nativeElement.children[0];
    this.component.name = this.id;
    let information: i_events_action = {
      dataset: this.dataset,
      element: this.component,
      event: "load",
      value: this.component,
    };
    this.desktop_mode = device.size.width >= 720 ? true : false;
    this.loaded.emit(information);
    /*setTimeout(() => {
      let high_viewport: number = device.size.height;
      this.component.style.maxHeight = `${high_viewport}px`;
    }, 400);*/
  };
}

