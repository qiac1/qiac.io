import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { TPWInterface } from 'ngx-typewriter';
import { i_events_action, i_devices } from 'src/app/elements/interfaces/common';
import { CommonService } from 'src/app/elements/services/common/common.service';
import { EncryptStorage } from 'storage-encryption';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  @Input() class!: string;
  @Input() id: string = "loader";

  @Input() placeholder: string = "common.t0022";

  @Output() loaded: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();

  component!: any;
  html_elements: any = {};
  dataset: any = {};
  desktop_mode: boolean = false;
  typewriter_options: TPWInterface = {
    textList: [],
    speed: 80,
    loop: true,
    delay: 200,
  }

  constructor(
    private s_common: CommonService,
    private s_element_ref: ElementRef,
    private s_translate: TranslateService,
  ) { }

  ngOnInit(): void {
    this.id = `uiux__${this.id}`;
    this.dataset = {
      class: this.class,
      id: this.id,
      placeholder: this.placeholder,
    };
    let placeholder = this.s_translate.instant(this.placeholder) ?? this.placeholder;
    this.typewriter_options.textList = [placeholder];
    this.on_resize(null);
  }

  on_resize(event: any): void {
    let device: i_devices = this.s_common.get_device();
    this.component = this.s_element_ref.nativeElement.children[0];
    this.component.name = this.id;
    let information: i_events_action = {
      dataset: this.dataset,
      element: this.component,
      event: "load",
      value: this.component,
    };
    this.desktop_mode = device.size.width >= 720 ? true : false;
    this.loaded.emit(information);
    /*setTimeout(() => {
      let high_viewport: number = device.size.height;
      this.component.style.maxHeight = `${high_viewport}px`;
    }, 400);*/
  };
}
