import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { i_events_action, i_devices, i_data_send } from 'src/app/elements/interfaces/common';
import { CommonService } from 'src/app/elements/services/common/common.service';
import { EncryptStorage } from 'storage-encryption';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  @Input() class: string = "";
  @Input() id: string = "button";

  @Input() alignment: string = "left";
  @Input() code_icon_off!: string;
  @Input() code_icon_on!: string;
  @Input() deactivated: boolean = false;
  @Input() icon_code!: string;
  @Input() name!: string;
  @Input() placeholder: string = "common.t0003";
  @Input() placeholder_off!: string;
  @Input() placeholder_on!: string;
  @Input() radio_border: string = "var(--spacing_xs)";
  @Input() style: string = "white";
  @Input() tooltip!: string;

  @Output() loaded: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();
  @Output() clicked: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();

  component!: any;
  html_elements: any = {};
  dataset: any = {};
  desktop_mode: boolean = false;

  button_type: string = "only_text";
  change_icon: boolean = false;
  css_styles = {
    "border-radius": "var(--spacing_xs)",
  };
  switch_mode: boolean = false;

  constructor(
    private s_common: CommonService,
    private s_element_ref: ElementRef,
    private s_translate: TranslateService,
  ) { }

  ngOnInit(): void {
    this.id = `uiux__${this.id}`;
    this.dataset = {
      class: this.class,
      code_icon_off: this.code_icon_off,
      code_icon_on: this.code_icon_on,
      deactivated: this.deactivated,
      icon_code: this.icon_code,
      id: this.id,
      name: this.name,
      placeholder: this.placeholder,
      placeholder_off: this.placeholder_off,
      placeholder_on: this.placeholder_on,
      radio_border: this.radio_border,
      style: this.style,
      tooltip: this.tooltip,
    };
    this.on_resize(null);

    this.alignment = this.alignment ? this.alignment.toLowerCase() : this.alignment;
    this.code_icon_off = this.code_icon_off ?? this.icon_code;
    this.switch_mode = this.code_icon_on ? true : false;
    this.code_icon_on = this.code_icon_on ?? this.icon_code;
    this.placeholder_off = this.placeholder_off ?? this.placeholder;
    this.placeholder_on = this.placeholder_on ?? this.placeholder;
    this.css_styles['border-radius'] = this.radio_border;
    this.tooltip = this.tooltip ?? this.placeholder;
    this.button_type = (this.icon_code || this.code_icon_off || this.code_icon_on) && (!this.placeholder || !this.placeholder_off || !this.placeholder_on) ? "only_icon" : this.button_type;
    this.button_type = (this.icon_code || this.code_icon_off || this.code_icon_on) && (this.placeholder || this.placeholder_off || this.placeholder_on) ? "with_icon" : this.button_type;
  }

  on_click(): void {
    if (!this.deactivated) {
      setTimeout(() => {
        this.change_icon = !this.change_icon ? true : false;
        this.icon_code = this.change_icon ? this.code_icon_on : this.code_icon_off;
        this.placeholder ? this.placeholder = this.change_icon ? this.placeholder_on : this.placeholder_off : null;
        let information: i_events_action = {
          dataset: this.dataset,
          element: this.component,
          event: "click",
          value: true,
        };
        information.value = this.switch_mode ? this.change_icon : information.value;
        this.clicked.emit(information);
      }, 400);
    }
  };

  on_resize(event: any): void {
    let device: i_devices = this.s_common.get_device();
    this.component = this.s_element_ref.nativeElement.children[0];
    this.component.name = this.id;
    let information: i_events_action = {
      dataset: this.dataset,
      element: this.component,
      event: "load",
      value: this.component,
    };
    this.desktop_mode = device.size.width >= 720 ? true : false;
    this.loaded.emit(information);
    /*setTimeout(() => {
      let high_viewport: number = device.size.height;
      this.component.style.maxHeight = `${high_viewport}px`;
    }, 400);*/
  };
}
