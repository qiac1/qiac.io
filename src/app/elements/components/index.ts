import { ConversationComponent } from "./for_modules/conversation/conversation.component";
import { HelpMenuComponent } from "./for_modules/help-menu/help-menu.component";
import { ProfileMenuComponent } from "./for_modules/profile-menu/profile-menu.component";
import { RequestComponent } from "./for_modules/request/request.component";
import { SettingsMenuComponent } from "./for_modules/settings-menu/settings-menu.component";
import { WindowChatComponent } from "./for_modules/window-chat/window-chat.component";

import { LoaderConversationComponent } from "./for_modules/loaders/loader-conversation/loader-conversation.component";

import { AvatarComponent } from "./ui_ux/avatar/avatar.component";
import { ButtonComponent } from "./ui_ux/button/button.component";
import { LoaderComponent } from "./ui_ux/loader/loader.component";
import { SelectionComponent } from "./ui_ux/selection/selection.component";

export const components: any[] = [
    /*For modules*/
    ConversationComponent,
    HelpMenuComponent,
    ProfileMenuComponent,
    RequestComponent,
    SettingsMenuComponent,
    WindowChatComponent,
    /* End*/

    /*Loaders*/
    LoaderConversationComponent,
    /* End*/

    /*UI UX*/
    AvatarComponent,
    ButtonComponent,
    LoaderComponent,
    SelectionComponent,
    /* END*/

];

export * from "./for_modules/conversation/conversation.component";
export * from "./for_modules/help-menu/help-menu.component";
export * from "./for_modules/profile-menu/profile-menu.component";
export * from "./for_modules/request/request.component";
export * from "./for_modules/settings-menu/settings-menu.component";
export * from "./for_modules/window-chat/window-chat.component";

export * from "./for_modules/loaders/loader-conversation/loader-conversation.component";

export * from "./ui_ux/avatar/avatar.component";
export * from "./ui_ux/loader/loader.component";
export * from "./ui_ux/button/button.component";
export * from "./ui_ux/selection/selection.component";