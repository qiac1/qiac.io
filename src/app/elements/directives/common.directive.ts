import { Directive, ElementRef, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from '../services/common/common.service';

@Directive({
  selector: '[common]'
})
export class CommonDirective {
  @Input("tooltip") tooltip!: string;

  constructor(
    private element_ref: ElementRef,
    private s_common: CommonService,
    private s_translate: TranslateService,) { }

  ngOnInit() {
    if (this.tooltip) {
      let tooltip = this.s_translate.instant(this.tooltip) ?? this.tooltip;
      this.element_ref.nativeElement.title = tooltip;
    };
  }
}
