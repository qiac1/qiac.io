import { TestBed } from '@angular/core/testing';

import { ChatHmService } from './chat-hm.service';

describe('ChatHmService', () => {
  let service: ChatHmService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChatHmService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
