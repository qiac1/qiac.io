import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { EncryptStorage } from 'storage-encryption';
import { CommonService } from '../../common/common.service';
import { i_conversation, i_document, i_request_ask_questions, i_answer } from 'src/app/elements/interfaces/for_modules';
import { i_file } from 'src/app/elements/interfaces/common';

@Injectable({
  providedIn: 'root'
})
export class ChatHmService {
  private session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  private local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  private token: any = "";

  constructor(
    private s_common: CommonService,
    private s_http: HttpClient,
    private s_translate: TranslateService,
  ) { }

  async create(api_key: any, request: i_request_ask_questions): Promise<any> {
    let url = environment.api_backend + api_key;
    //let url = "http://localhost:3000/ask-questions_get";
    let all_items: any;
    let items = <any>{};
    this.token = environment.token;
    if (this.token) {
      const headers = new HttpHeaders().append("Authorization", `Token ${this.token}`).append("Content-Type", "application/json");
      try {
        all_items = await this.s_http.post<any>(url, request, { headers: headers }).toPromise();
        //all_items = await this.s_http.get<any>(url).toPromise();
        items = this.formatting_request(all_items);
        /*let response = Array<i_answer>();
        for (let a = 0; a < all_items.length; a++) {
          const answer: any = all_items[a];
          let item: i_answer = {
            answer: answer.data.result.replace("Inteligencia SQDM:", "") ?? answer.data.result,
            context: answer.data.source_documents[0][0][1],
            id: answer.id ?? this.s_common.guid_generator(),
            question: answer.data.query,
          };
          response.push(item);
        };
        return response*/
        return items;
      } catch (error: any) {
        return items;
      };
    } else {
      return items;
    };
  }

  async create_document(request: i_file): Promise<any> {
    let url = environment.api_backend;
    //let url = "http://localhost:3000/documents_create";
    let all_items: any;
    let items = <any>{};
    this.token = environment.token;
    if (this.token) {
      const headers = new HttpHeaders().append("Authorization", `Token ${this.token}`);
      try {
        let format_data: any = new FormData();
        format_data.append("name", request.name);
        format_data.append("file", request.blob);
        all_items = await this.s_http.post<any>(url, format_data, { headers: headers }).toPromise();
        //all_items = await this.s_http.get<any>(url).toPromise();
        items = this.formatting_documents(all_items);
        return items;
      } catch (error: any) {
        return items;
      };
    } else {
      return items;
    };
  }

  formatting_conversations(db: any): any {
    let response = Array<i_conversation>();
    if (this.token == "mockup") {
      for (let a = 0; a < db.length; a++) {
        const to_format = db[a];
        let item: i_conversation = {
          actions: {
            delete: true,
            read: true,
            update: true,
          },
          creation_date: {
            placeholder: to_format.creation_date ? this.s_common.serialize_date(to_format.creation_date, "LL") : this.s_common.serialize_date(new Date(), "LL"),
            value: to_format.creation_date ? to_format.creation_date : new Date(),
          },
          id: to_format.id,
          name: to_format.name ? this.s_common.to_capitalize_first_letter(to_format.name) : to_format.name,
          answers: to_format.requests,
          show_hide: true,
        };
        response.push(item);
      }
    } else {

    }
    return response;
  };

  formatting_documents(db: any): any {
    let response = <i_file>{};
    let item: i_file = {
      blob: db.file,
      id: db.id,
      name: db.name,
    };
    response = item;
    return response;
  };

  formatting_document_list(db: any[]): any {
    let response = Array<i_document>();
    for (let a = 0; a < db.length; a++) {
      const document: any = db[a];
      let item: i_document = {
        file: document.file,
        id: document.id,
        name: document.name,
        selected: document.selected ?? false,
        show_hide: document.show ?? true,
      };
      response.push(item);
    };
    return response;
  };

  formatting_request(db: any): any {
    let response = <i_answer>{};
    let item: i_answer = {
      answer: db.data.result.replace("Inteligencia SQDM:", "") ?? db.data.result,
      context: db.data.source_documents[0][0][1],
      id: db.id ?? this.s_common.guid_generator(),
      question: db.data.query,
    };
    response = item ?? response;
    return response;
  };

  async get_conversations(id?: any): Promise<any> {
    let api_key: string = environment.key_conversations;
    let response: any = await this.read().catch((result) => { });
    return response;
  };

  async get_documents(): Promise<any> {
    let response: any = await this.read().catch((result) => { });
    return response;
  };

  async set_chat(request: i_request_ask_questions): Promise<any> {
    let api_key: string = environment.key_question;
    let response: any = await this.create(api_key, request).catch((result) => { });
    return response;
  };

  async set_document(request: i_file): Promise<any> {
    let response: any = await this.create_document(request).catch((result) => { });
    return response;
  };

  async read(): Promise<any> {
    //let url = "http://localhost:3000/documents_get";
    let url = environment.api_backend;
    let all_items: any;
    this.token = environment.token;
    if (this.token) {
      const headers = new HttpHeaders().append("Authorization", `Token ${this.token}`);
      try {
        let formatted_items: any = []
        all_items = await this.s_http.get<any>(url, { headers: headers }).toPromise();
        //all_items = await this.s_http.get<any>(url).toPromise();
        formatted_items = this.formatting_document_list(all_items);
        return formatted_items;
      } catch (error: any) {
        return all_items;
      };
    } else {
      return all_items;
    };
  }
}
