import { EventEmitter, Injectable, Output } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { EncryptStorage } from 'storage-encryption';
import * as XLSX from 'xlsx';
import * as moment from 'moment';
import { DeviceDetectorService } from 'automatica-ngx-device-detector';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable, ReplaySubject } from 'rxjs';
import { NgxTypewriterService, TPW, TPWInterface } from 'ngx-typewriter';
import { i_colors, i_columns, i_columns_pagination, i_country, i_data_send, i_devices, i_format_date, i_hsl, i_rgb } from '../../interfaces/common';

import * as common_json from './../../../../assets/json/common.json';
import * as countries_json from './../../../../assets/json/countries.json';
import * as lists_json from './../../../../assets/json/lists.json';

declare const window: any;
@Injectable({
  providedIn: 'root'
})
export class CommonService {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  @Output() data_send: EventEmitter<any> = new EventEmitter<any>();

  private authenticated: boolean = false;
  public content_aux: any = {
    es: {
      a000: [
        {
          placeholder: "opción #1",
          value: 0,
        },
        {
          placeholder: "opción #2",
          value: 1,
        },
      ],
      a001: [
        {
          code_name: 0,
          placeholder: "columna #1",
        },
        {
          code_name: 1,
          placeholder: "columna #2",
        },
        {
          code_name: 2,
          placeholder: "columna #3",
        },
      ],
    },
    en: [
      {
        placeholder: "option #1",
        value: 0,
      },
      {
        placeholder: "option #2",
        value: 1,
      },
    ],
    a001: [
      {
        code_name: 0,
        placeholder: "column #1",
      },
      {
        code_name: 1,
        placeholder: "column #2",
      },
      {
        code_name: 2,
        placeholder: "column #3",
      },
    ],
  };
  public corsDecode: string = "https://cors-server-sqdm.fly.dev/"
  public default_language!: any;
  public elems!: any;
  public translation!: any;

  constructor(
    private s_device_detector: DeviceDetectorService,
    private s_translate: TranslateService,
    private s_title: Title,
    private s_route: Router,
    private s_http: HttpClient,
    private s_typewriter: NgxTypewriterService,

  ) {
    this.default_language = this.session_data.decrypt("language");
  }

  current_option(array: any[], parameter: string, value: any) {
    if (array) {
      for (let a = 0; a < array.length; a++) {
        const elem = array[a];
        if (elem[parameter] == value) {
          return elem;
          break;
        };
      };
    };
  };

  add_zero(x: any, n: any): any {
    while (x.toString().length < n) {
      x = "0" + x;
    }
    return x;
  };

  apply_mode(mode: string) {
    let html: any = document.getElementsByTagName('html')[0] ?? null;
    mode = mode ? mode : "auto";
    let saved_mode = this.local_data.decrypt("mode") ?? null;
    if (mode != "auto") {
      if (saved_mode) {
        html.classList.remove(saved_mode);
        document.body.classList.remove(saved_mode);
        html.classList.add(mode);
        document.body.classList.add(mode);
        this.local_data.encrypt("mode", mode);
      } else {
        this.local_data.encrypt("mode", mode);
        html.classList.add(mode);
        document.body.classList.add(mode);
      };
    } else {
      if (saved_mode) {
        document.body.classList.remove(saved_mode);
        html.classList.remove(saved_mode);
        document.body.classList.add(saved_mode);
        html.classList.add(saved_mode);
        this.local_data.encrypt("mode", mode);
      } else {
        this.local_data.encrypt("mode", "light");
        html.classList.add("auto");
        document.body.classList.add("auto");
      };
    }
  };

  build_archive(files: any) {
    let file_reader = new FileReader();
    file_reader.onload = () => {
      let fileURL = file_reader.result;
    }
    file_reader.readAsDataURL(files);
  }

  calculate_color(hex: any): i_colors {
    let color: i_colors = <i_colors>{};
    color.hex = hex.toUpperCase();
    color.rgb = this.hex_to_rgb(color.hex);
    color.hsl = this.rgb_to_hsl(color.rgb);
    return color;
  };

  calculate_percentage_between_ranges(minimum: number, maximum: number, range_value: any): number {
    let response;
    response = 100 * (range_value - minimum) / (maximum - minimum);
    return response;
  };

  blob_to_base64: any = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader;
    reader.onerror = reject;
    reader.onload = () => {
      resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });

  change_language(language_code: string) {
    this.s_translate.setDefaultLang(language_code);
    this.s_translate.use(language_code);
  };

  change_name_tab(name: string) {
    this.s_title.setTitle(name);
  };

  change_name_tab_translation(key_translation: string) {
    this.s_translate.get(key_translation).subscribe((response) => {
      let titleTab = response ? response : "";
      this.s_title.setTitle(titleTab);
    });
  };

  change_variable_CSS(name_variable: string, value: any) {
    document.body.style.setProperty(`${"--"}${name_variable}`, value);
  };

  convert_dataset_to_object(dataset: DOMStringMap): any {
    let response: any = {};
    let keys = Object.keys(dataset) ?? [];
    for (let a = 0; a < keys.length; a++) {
      const key: string = keys[a];
      response[key] = dataset[key];
    };
    return response;
  };

  convert_string_to_array(string: string, separate: string): any[] {
    let array = [];
    if (string.indexOf(separate) >= 0) {
      array = string.split(separate);
    } else {
      if (string.indexOf("[") >= 0 || string.indexOf("]") >= 0) {
        let convert_string = JSON.parse(string);
        array = convert_string;
      } else {
        array.push(string);
      }
    };
    return array;
  };

  convert_string_to_boolean(string: string): boolean {
    let response = string ? (string.toLowerCase() === 'true') : false;
    return response;
  };

  convert_string_to_rows(string: string, columns: any[]): any[] {
    let response: any[] = [];
    if (string) {
      if (string.indexOf("{") >= 0 || string.indexOf("}") >= 0 || string.indexOf("[") >= 0 || string.indexOf("]") >= 0) {
        let convert_string = JSON.parse(string);
        response = convert_string;
      } else {
        let array = string.split(",");
        for (let a = 0; a < array.length; a++) {
          let options: any = array[a];
          options = options.replace("(", "");
          options = options.replace(")", "");
          options = options.split(";");
          let item: any = {};
          for (let b = 0; b < options.length; b++) {
            const option = options[b];
            item[columns[b].code_name] = option;
          }
          response.push(item);
        };
      };
    } else {
      response = this.content_aux[this.default_language].a002;
    };
    return response;
  };

  convert_url_to_base64(url: string, callback: any): any {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
      var reader = new FileReader();
      reader.onloadend = function () {
        callback(reader.result);
      };
      reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', this.corsDecode + url);
    xhr.responseType = 'blob';
    xhr.send();
  };

  convert_url_to_file(data_url: string, file_name: string): File {
    let arr: any[] = data_url.split(','),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], file_name, {
      type: mime
    });
  };

  export_as_excel_file(array: any[], excel_file_name: string) {
    const worksheet = XLSX.utils.json_to_sheet(array);
    const workbook = {
      Sheets: {
        'data': worksheet
      },
      SheetNames: ['data']
    };
    XLSX.writeFile(workbook, this.to_export_file_name(excel_file_name));
  };

  file_to_string(file: File): Observable<string> {
    const result = new ReplaySubject<string>(1);
    const reader = new FileReader();
    reader.readAsBinaryString(file);
    reader.onload = (event: any) => result.next(btoa(event.target.result.toString()));
    return result;
  };

  filter_only_numbers_inputs(event: any): any {
    var key = event.charCode;
    return key >= 48 && key <= 57;
  };

  filter_only_text_inputs(value: any): any {
    let out = "";
    let value_in = value;
    let filter = "0123456789";
    if (value) {
      for (let index = 0; index < value_in.length; index++) {
        if (filter.indexOf(value_in.charAt(index)) == -1) {
          out += value_in.charAt(index);
        }
      }
    }
    return value_in = out;
  };

  format_date(date: string | Date): i_format_date {
    let response: i_format_date = <i_format_date>{};
    let is_date = typeof date;
    let date_in: any = null;
    date_in = is_date == "string" ? new Date(date) : date;
    let day = date_in.getDate();
    day <= 30 ? date_in.setDate(date_in.getDate() + 1) : null;
    if (date_in) {
      response.date_ISO = moment(date_in).format("L");
      response.day = moment(date_in).format("DD");
      response.full_date = this.serialize_date(date_in, "");
      response.hour = moment(date_in).format("HH");
      response.minutes = moment(date_in).format("mm");
      response.month = moment(date_in).format("MM");
      response.obj_date = date_in;
      response.seconds = moment(date_in).format("ss");
      response.year = moment(date_in).format("Y");
    }
    return response;
  };

  generate_id(): any {
    let id: any = null;
    let date: any = moment();
    date = date.format("YYYYMMDDHHMMSS");
    id = date;
    return id;
  };

  generate_name_sqdm(area: string, file_type: string, name: string,): string {
    let response: string = "";
    let date: any = moment();
    date = date.format("YYYY_MM_DD");
    let y = this.add_zero(new Date().getFullYear(), 2);
    area = area ? area.toUpperCase() : area;
    file_type = file_type ? this.to_capitalize_first_letter(file_type) : file_type;
    name = file_type ? this.to_capitalize_first_letter(name) : name;
    response = `${area}_ ${file_type} - ${name} _${date} _VR1`;
    return response;
  };

  get_browser(): string {
    let response: string = "chrome";
    const agent = window.navigator.userAgent.toLowerCase()
    switch (true) {
      case agent.indexOf('edge') > -1:
        response = 'edge';
        break;
      case agent.indexOf('opr') > -1 && !!(<any>window).opr:
        response = 'opera';
        break;
      case agent.indexOf('chrome') > -1 && !!(<any>window).chrome:
        response = 'chrome';
        break;
      case agent.indexOf('trident') > -1:
        response = 'ie';
        break;
      case agent.indexOf('firefox') > -1:
        response = 'firefox';
        break;
      case agent.indexOf('safari') > -1:
        response = 'safari';
        break;
      default:
        response = 'other';
        break;
    }
    return response;
  };

  get_countries(): any[] {
    let countries: any[] = (countries_json as any).default;
    let response: any[] = [];
    for (let a = 0; a < countries.length; a++) {
      const country: any = countries[a];
      let currency: string[] = country.currencies ? Object.keys(country.currencies) : [];
      let language: string[] = country.languages ? Object.keys(country.languages) : [];
      let formatting: i_country = {
        code: country.cca2,
        currencies: {
          abbreviation: currency[0],
          name: country.currencies ? country.currencies[currency[0]].name : "",
          symbol: country.currencies ? country.currencies[currency[0]].symbol : "",
        },
        flag: country.flag,
        language: {
          abbreviation: language[0] == "spa" ? "es" : "en",
          name: country.languages ? country.languages[language[0]] : "",
        },
        name: country.name.common,
        maps: country.maps,
      };
      response.push(formatting);
    };
    response = this.sort_ascent(response, "name") ?? response;
    return response;
  };

  get_data_json(key: string): any {
    let data!: any;
    switch (key) {
      case "stages":
        data = (lists_json as any).default["stages"];
        break;
      default:
        data = (common_json as any).default[key];
        break;
    };
    return data;
  };

  get_device(): i_devices {
    let response: i_devices = {
      size: {
        height: 0,
        width: 0,
      },
      device: {
        device: "",
        desktop: false,
        mobile: false,
        orientation: "",
        os: "",
        os_version: "",
        tablet: false,
      }
    }
    let device = this.s_device_detector.getDeviceInfo();
    let width = screen.width;
    let height = screen.height;
    response.size.height = height;
    response.size.width = width;
    response.device = {
      desktop: this.s_device_detector.isDesktop(),
      device: device.device,
      mobile: this.s_device_detector.isMobile(),
      orientation: device.orientation,
      os: device.os,
      os_version: device.os_version,
      tablet: this.s_device_detector.isTablet(),
    };
    return response;
  };

  get_rows_custom_page(rows: any[], items_per_page: number, page: any | number): any[] {
    let response: any[] = [];
    page = parseInt(page);
    let items_to_skip = (page - 1) * items_per_page;
    let items = rows.slice(items_to_skip, items_per_page + items_to_skip);
    response = items;
    return response;
  };

  get_scroll_active(container: any, content: any): any {
    let response = false;
    let difference = content.offsetHeight - container.offsetHeight;
    response = difference > 0 ? true : false;
    return response;
  };

  go_to(page: string) {
    if (page.indexOf("/") >= 0) {
      this.s_route.navigate([page]);
    } if (page.toLowerCase() == "back") {
      history.go(-1);
    } else {
      page = `/${page}`
      this.s_route.navigate([page]);
    }
  };

  go_to_link(link: any, target: string) {
    window.open(link, target);
  }

  go_to_anchor(query_selector: string) {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
    window.scrollBy({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
    this.elems.anchor_element = document.querySelector(query_selector);
    this.elems.anchor_element.scrollIntoView({
      behavior: "smooth"
    });
  };

  go_to_top() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
    window.scrollBy({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
    document.body.scrollIntoView({
      behavior: "smooth"
    });
  };

  go_to_with_params(page: string, data: any) {
    this.s_route.navigate([page], { queryParams: data });
  };

  guid_generator() {
    var d = new Date().getTime();
    var d2 = ((typeof performance !== 'undefined') && performance.now && (performance.now() * 1000)) || 0;
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16;
      if (d > 0) {
        r = (d + r) % 16 | 0;
        d = Math.floor(d / 16);
      } else {
        r = (d2 + r) % 16 | 0;
        d2 = Math.floor(d2 / 16);
      }
      return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
  };

  hex_to_rgb(hex_color: any): i_rgb {
    let response: i_rgb = <i_rgb>{};
    let r = Number('0x' + hex_color.substr(1, 2))
    let g = Number('0x' + hex_color.substr(3, 2))
    let b = Number('0x' + hex_color.substr(5, 2))
    let rgb = [r, g, b]
    let rgbString = 'rgb(' + r + ', ' + g + ', ' + b + ')'
    response.array = rgb;
    response.string = rgbString.toUpperCase();
    return response;
  };

  hsl_to_hex(h: any, s: any, l: any): string {
    let response: string = "";
    l /= 100;
    const a = s * Math.min(l, 1 - l) / 100;
    const f = (n: any) => {
      const k = (n + h / 30) % 12;
      const color = l - a * Math.max(Math.min(k - 3, 9 - k, 1), -1);
      return Math.round(255 * color).toString(16).padStart(2, '0');   // convert to Hex and prefix "0" if needed
    };
    response = `#${f(0)}${f(8)}${f(4)}`;
    return response;
  };

  invest_array(array: any[]): any[] {
    let response: any[] = [];
    for (let a = 0; a < array.length / 2; a++) {
      let temporary = array[a];
      let counter_index = array.length - a - 1;
      array[a] = array[counter_index];
      array[counter_index] = temporary;
    };
    response = array;
    return response;
  };

  paging_tables(rows: any[], items_per_page: number): any {
    let response: any = {
      number_of_pages: 0,
      rows: [],
      total_rows: 0,
    };
    let number_of_pages: any = rows.length / items_per_page;
    number_of_pages = number_of_pages.toFixed(0);
    for (let a = 0; a < rows.length; a++) {
      const row = rows[a];
      let items_to_skip = ((a + 1) - 1) * items_per_page;
      let items = rows.slice(items_to_skip, items_per_page + items_to_skip);
      response.number_of_pages = number_of_pages;
      response.rows.push(items);
      response.total_rows = rows.length;
    };
    return response;
  };

  prohibit_input_spaces(value: any): any {
    let value_in = value;
    return value_in.replace(/ /g, "");
  };

  replace_input_spaces(event: any, replace: string) {
    let value = event.target.value;
    event.target.value = value.replace(/ /g, replace);
  };

  rgb_to_hex(r: number, g: number, b: number): string {
    let response: string = "#" + (1 << 24 | r << 16 | g << 8 | b).toString(16).slice(1);
    return response;
  };

  rgb_to_hsl(rgb: i_rgb): i_hsl {
    let response: i_hsl = <i_hsl>{};
    let h!: any, s!: any, l!: any;
    let strongestChannel = 0;
    let normalizedRgb = rgb.array.map((n) => {
      return n / 255
    });
    let [r, g, b] = normalizedRgb;
    let minVal = normalizedRgb.reduce((t, v) => {
      return Math.min(t, v)
    });
    let maxVal = normalizedRgb.reduce((t, v, i) => {
      if (v > t) {
        strongestChannel = i
        return v
      } else {
        return t
      }
    });
    l = (minVal + maxVal) / 2;
    if (maxVal != minVal) {
      if (l > 50) {
        s = (maxVal - minVal) / (2 - maxVal - minVal);
      } else {
        s = (maxVal - minVal) / (maxVal + minVal);
      }
      switch (strongestChannel) {
        case 0:
          h = (g - b) / (maxVal - minVal);
          break
        case 1:
          h = 2 + (b - r) / (maxVal - minVal);
          break
        case 2:
          h = 4 + (r - g) / (maxVal - minVal);
      }
      h = h * 60;
      if (h < 0) {
        h += 360;
      }
    } else {
      s = 0;
      h = 0;
    }
    h = Math.round(h);
    s = Math.round(s * 100);
    l = Math.round(l * 100);
    let string = 'hsl(' + h + ', ' + s + '%, ' + l + '%)';
    response.array = [h, s, l];
    response.string = string.toUpperCase();
    return response
  };

  remove_duplicated_in_array(array: any[], by_parameter: string): any[] {
    let hash: any = {};
    let formate: any[] = array.filter((current: any) => {
      let exists = !hash[current[by_parameter]];
      hash[current[by_parameter]] = true;
      return exists;
    });
    return formate ? formate : [];
  };

  remove_in_array(array: any[], by_parameter: string, value_to_remove: any): any[] {
    let formate: any = [];
    formate = array.filter((item: any) => item[by_parameter] !== value_to_remove);
    return formate;
  };

  scroll_into_element(element: any, scroll_left: number, scroll_top: number) {
    if (element) {
      scroll_left = scroll_left ? scroll_left : 0;
      scroll_top = scroll_top ? scroll_top : 0;
      element.scrollLeft = scroll_left;
      element.scrollTop = scroll_top;
    };
  };

  search_in_array(array: any[], search_parameter: string, search_value: any): any[] {
    let in_array: any[] = array;
    let search = in_array.filter((elem: any) => elem[search_parameter] ? elem[search_parameter].indexOf(search_value) >= 0 : false);
    return search ? search : [];
  };

  search_exactly_in_array(array: any[], search_parameter: string, search_value: any): any {
    let in_array: any[] = array;
    let search: any[] = in_array.filter((elem: any) => elem[search_parameter] ? elem[search_parameter] == search_value : false);
    return search ? search.length > 0 ? search[0] : null : null;
  };

  send_data(data: i_data_send): void {
    this.data_send.emit(data);
  };

  serialize_date(date: any, format_date: string): string {
    let result;
    format_date = format_date ? format_date : "LLLL";
    let is_date = typeof date;
    let date_in: any = null;
    date_in = is_date == "string" ? new Date(date) : date;
    let day = date_in.getDate();
    day <= 30 ? date_in.setDate(date_in.getDate() + 1) : null;
    moment.locale(this.default_language);
    result = moment(date_in).format(format_date);
    return result;
  };

  serialize_time(time: any, format_time: string): string {
    let result;
    format_time = format_time ? format_time : "LT";
    moment.locale(this.default_language);
    result = moment(time, "HH:mm").format(format_time);
    return result;
  };

  show_notification(title: string, text: string, url_open: string, translation: boolean): void {
    if (translation) {
      this.s_translate.get(title).subscribe((response: string) => {
        title = response;
        title = this.to_capitalize_first_letter(title);
        this.s_translate.get(text).subscribe((response: string) => {
          text = response;
          text = this.to_capitalize_first_letter(text);
          if (Notification.permission !== 'granted') {
            Notification.requestPermission();
          } else {
            var notification = new Notification(title, {
              body: text,
            });
            if (url_open) {
              notification.onclick = function () {
                window.open(url_open);
              };
            };
          };
        }, (error: any) => {
          text = text;
        });
      }, (error: any) => {
        title = title;
      });
    } else {
      if (Notification.permission !== 'granted') {
        Notification.requestPermission();
      } else {
        title = this.to_capitalize_first_letter(title);
        text = this.to_capitalize_first_letter(text);
        var notification = new Notification(title, {
          body: text,
        });
        if (url_open) {
          notification.onclick = function () {
            window.open(url_open);
          };
        };
      };
    }
  };

  shuffle(array: any[]) {
    var j, x, i;
    for (i = array.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      x = array[i];
      array[i] = array[j];
      array[j] = x;
    };
  };

  split_words(text: string, length: number): string {
    var len = text.length,
      re = /[ ,.]/,
      from = (0 <= 0) ? 0 : text.substr(0).search(re) + 0 + 1,
      to = (length >= len) ? len : text.substr(length).search(re) + length;
    if (from === -1) {
      from = 0;
    }
    if (to === (length - 1)) {
      to = len;
    }
    return text.substr(from, to);
  }

  sort_ascent(array: any[], by: string): any[] {
    let response: any = [];
    response = array.sort((a: any, b: any) => {
      if (a[by] < b[by]) {
        return -1;
      } else if (a[by] > b[by]) {
        return 1;
      } else {
        return 0;
      }
    });
    return response;
  };

  sort_ascent_date(array: any[], parameter_name: string): any[] {
    let response: any[] = [];
    response = array.sort((a: any, b: any) => new Date(b[parameter_name]).getTime() + new Date(a[parameter_name]).getTime());
    return response;
  };

  sort_decent(array: any, by: string): any[] {
    let response: any[] = [];
    response = array.reverse((a: any, b: any) => {
      if (a[by] < b[by]) {
        return -1;
      } else if (a[by] > b[by]) {
        return 1;
      } else {
        return 0;
      }
    });
    return response;
  };

  sort_decent_2G(array: any, by: string, sub: string) {
    let response: any[] = [];
    response = array.reverse((a: any, b: any) => {
      if (a[by][sub] < b[by][sub]) {
        return -1;
      } else if (a[by][sub] > b[by][sub]) {
        return 1;
      } else {
        return 0;
      }
    });
    return response;
  };

  sort_decent_date(array: any[], parameter_name: string): any[] {
    let response: any = [];
    response = array.sort((a, b) => new Date(b[parameter_name]).getTime() - new Date(a[parameter_name]).getTime());
    return response;
  };

  separate_array_columns(array: any[]): i_columns {
    let response = <i_columns>{};
    response.all = array;
    response.columns = <i_columns_pagination>{};
    response.columns.left = [];
    response.columns.right = [];
    response.count = array.length;
    let half = Math.floor(array.length / 2);
    response.columns.left = array.slice(0, half);
    response.columns.right = array.slice(half);
    return response;
  };

  to_capitalize(text: string): string {
    let textArray = text.split(" ");
    let capitalize = "";
    let response!: string;
    if (textArray.length > 0) {
      for (let a = 0; a < textArray.length; a++) {
        let lower = textArray[a].toLowerCase();
        let out = textArray[a].charAt(0).toUpperCase() + lower.slice(1);
        if (a == textArray.length - 1) {
          capitalize = capitalize + out;
        } else {
          capitalize = capitalize + out + " ";
        }
      }
      response = capitalize;
    }
    return response;
  };

  to_capitalize_first_letter(text: string): string {
    return text.charAt(0).toUpperCase() + text.slice(1);
  };

  to_export_file_name(excelFileName: string) {
    return `${excelFileName}_export_${new Date().getTime()}.xlsx`;
  };
}
