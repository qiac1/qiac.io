import { i_date } from "./common";

export interface i_actions {
    read: boolean;
    update: boolean;
    delete: boolean;
}

export interface i_conversation {
    actions: i_actions;
    creation_date: i_date;
    id: any;
    name: string;
    answers: i_answer[];
    show_hide?: boolean;
}

export interface i_document {
    file: any,
    id: any,
    name: string,
    selected?: boolean,
    show_hide?: boolean;
}

export interface i_answer {
    answer: string;
    context?: any;
    id?: any;
    question: string;
}

export interface i_stages {
    hide?: boolean;
    id: any;
    placeholder: string;
    placeholder_on: string;
    selected: boolean,
}

export interface i_request_ask_questions {
    document_id?: string;
    question: string;
}